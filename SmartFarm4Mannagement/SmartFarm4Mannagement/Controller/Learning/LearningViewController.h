//
//  LearningViewController.h
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LearningCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@protocol chatDelegate
-(void)getStrChat:(NSString *)strChat;
@end
@interface LearningViewController : UIViewController<UICollectionViewDragDelegate,UICollectionViewDataSource>
{
    NSArray *arrLearning;
}
@property (weak, nonatomic) id chatDelegate;
//header
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
- (IBAction)btnHomeClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
- (IBAction)btnSettingClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;
- (IBAction)btnChatClick:(id)sender;

//content
@property (weak, nonatomic) IBOutlet UICollectionView *collectLearning;

@end

NS_ASSUME_NONNULL_END
