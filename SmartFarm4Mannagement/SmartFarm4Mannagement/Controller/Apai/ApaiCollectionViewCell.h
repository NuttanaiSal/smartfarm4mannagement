//
//  ApaiCollectionViewCell.h
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApaiCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgApai;

@end

NS_ASSUME_NONNULL_END
