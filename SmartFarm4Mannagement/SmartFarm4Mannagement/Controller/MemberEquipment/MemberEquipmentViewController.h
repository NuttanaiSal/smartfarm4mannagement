//
//  MemberEquipmentViewController.h
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MemberEquipmentTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@protocol chatDelegate
-(void)getStrChat:(NSString *)strChat;
@end
@interface MemberEquipmentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrMemberEuipment;
    NSMutableDictionary *dictMemberEuipment;
    NSString *strTopic,*strDetail,*strGroup,*strStatusComplete,*strStatusProcess,*strStatusWait;
    NSArray *arrMemberEuipmentRow;
    NSDictionary *dictMemberEuipmentRow;
}
@property (weak, nonatomic) id chatDelegate;
//header
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
- (IBAction)btnHomeClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
- (IBAction)btnSettingClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;
- (IBAction)btnChatClick:(id)sender;

//content
@property (weak, nonatomic) IBOutlet UITableView *tableMemberEquipment;

@end

NS_ASSUME_NONNULL_END
