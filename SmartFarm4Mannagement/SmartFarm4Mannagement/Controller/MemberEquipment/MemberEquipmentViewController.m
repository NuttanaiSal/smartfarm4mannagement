//
//  MemberEquipmentViewController.m
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import "MemberEquipmentViewController.h"

@interface MemberEquipmentViewController ()

@end

@implementation MemberEquipmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setData];
    [self setButtonRadius:self.btnHome radius:90];
    [self setButtonRadius:self.btnSetting radius:90];
    [self setButtonRadius:self.btnChat radius:90];
}
- (void)setButtonRadius: (UIButton *)button radius: (int)cornerRadius {
    button.layer.cornerRadius = cornerRadius;
    button.clipsToBounds = YES;
}
-(void)setData{
    arrMemberEuipment = [NSMutableArray array];
    dictMemberEuipment = [NSMutableDictionary dictionary];
    for (int i=0; i<10; i++) {
        strTopic = [NSString stringWithFormat:@"ต้องการกำลังสมาชิก (%d)",i];
        strDetail = [NSString stringWithFormat:@"ช่วยเตรียมแปลงเพื่อลงต้นกล้า (%d)",i];
        strGroup = [NSString stringWithFormat:@"สมาชิกกลุ่มสมุนไพรดงบัง %d",i+12345];
        if (i<3) strStatusProcess = @"YES";
        else strStatusProcess = @"NO";
        if (i<6&&i>=3) strStatusWait = @"YES";
        else strStatusWait = @"NO";
        if (i>=6) strStatusComplete = @"YES";
        else strStatusComplete = @"NO";
        [dictMemberEuipment setValue:strTopic forKey:@"topic"];
        [dictMemberEuipment setValue:strDetail forKey:@"detail"];
        [dictMemberEuipment setValue:strGroup forKey:@"group"];
        [dictMemberEuipment setValue:strStatusComplete forKey:@"statusComplete"];
        [dictMemberEuipment setValue:strStatusProcess forKey:@"statusProcess"];
        [dictMemberEuipment setValue:strStatusWait forKey:@"statusWait"];
        [arrMemberEuipment addObject:dictMemberEuipment];
        dictMemberEuipment = [NSMutableDictionary dictionary];
    }
    [_tableMemberEquipment reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrMemberEuipment.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *simpleTableIdentifier = @"memberEquipmentCell";
    
    MemberEquipmentTableViewCell *cell = (MemberEquipmentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[MemberEquipmentTableViewCell alloc]initWithStyle:
        UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    dictMemberEuipmentRow = [arrMemberEuipment objectAtIndex:indexPath.row];
    cell.lblTopic.text = [dictMemberEuipmentRow objectForKey:@"topic"];
    cell.lblDetail.text = [dictMemberEuipmentRow objectForKey:@"detail"];
    cell.lblGroup.text = [dictMemberEuipmentRow objectForKey:@"group"];
    if ([[dictMemberEuipmentRow objectForKey:@"statusComplete"] isEqualToString:@"YES"])
        cell.lblStatusComplete.textColor = [UIColor greenColor];
    else
        cell.lblStatusComplete.textColor = [UIColor grayColor];
    if ([[dictMemberEuipmentRow objectForKey:@"statusProcess"] isEqualToString:@"YES"])
        cell.lblStatusProcess.textColor = [UIColor orangeColor];
    else
        cell.lblStatusProcess.textColor = [UIColor grayColor];
    if ([[dictMemberEuipmentRow objectForKey:@"statusWait"] isEqualToString:@"YES"])
        cell.lblStatusWait.textColor = [UIColor redColor];
    else
        cell.lblStatusWait.textColor = [UIColor grayColor];
    return cell;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnHomeClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
    [self.chatDelegate getStrChat:@"NO"];
}
- (IBAction)btnSettingClick:(id)sender {
}
- (IBAction)btnChatClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.chatDelegate getStrChat:@"YES"];
}
@end
