//
//  TreeLogisViewController.m
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import "TreeLogisViewController.h"

@interface TreeLogisViewController ()

@end

@implementation TreeLogisViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setData];
    [self setButtonRadius:self.btnHome radius:90];
    [self setButtonRadius:self.btnSetting radius:90];
    [self setButtonRadius:self.btnChat radius:90];
    
}
- (void)setButtonRadius: (UIButton *)button radius: (int)cornerRadius {
    button.layer.cornerRadius = cornerRadius;
    button.clipsToBounds = YES;
}
-(void)setData{
    arrTreeLogis = [NSMutableArray array];
    dictTreeLogis = [NSMutableDictionary dictionary];
    for (int i=0; i<10; i++) {
        strTopic = [NSString stringWithFormat:@"ต้องการแลกฟ้าทลายโจร (%d)",i];
        strDetail = [NSString stringWithFormat:@"แลกกับต้นกล้า ขิง(%d)",i];
        strGroup = [NSString stringWithFormat:@"สมาชิกกลุ่มสมุนไพรดงบัง %d",i+12345];
        if (i<5&&i!=0) strStatusProcess = @"YES";
        else strStatusProcess = @"NO";
        if (i>=5) strStatusWait = @"YES";
        else strStatusWait = @"NO";
        [dictTreeLogis setValue:strTopic forKey:@"topic"];
        [dictTreeLogis setValue:strDetail forKey:@"detail"];
        [dictTreeLogis setValue:strGroup forKey:@"group"];
        [dictTreeLogis setValue:strStatusProcess forKey:@"statusProcess"];
        [dictTreeLogis setValue:strStatusWait forKey:@"statusWait"];
        [arrTreeLogis addObject:dictTreeLogis];
        dictTreeLogis = [NSMutableDictionary dictionary];
    }
    [_tableTreeLogis reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrTreeLogis.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *simpleTableIdentifier = @"treeLogisCell";
    
    TreeLogisTableViewCell *cell = (TreeLogisTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[TreeLogisTableViewCell alloc]initWithStyle:
        UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    dictTreeLogisRow = [arrTreeLogis objectAtIndex:indexPath.row];
    cell.lblTopic.text = [dictTreeLogisRow objectForKey:@"topic"];
    cell.lblDetail.text = [dictTreeLogisRow objectForKey:@"detail"];
    cell.lblGroup.text = [dictTreeLogisRow objectForKey:@"group"];
    if ([[dictTreeLogisRow objectForKey:@"statusProcess"] isEqualToString:@"YES"])
        cell.lblStatusProcess.textColor = [UIColor orangeColor];
    else
        cell.lblStatusProcess.textColor = [UIColor grayColor];
    if ([[dictTreeLogisRow objectForKey:@"statusWait"] isEqualToString:@"YES"])
        cell.lblStatusWait.textColor = [UIColor redColor];
    else
        cell.lblStatusWait.textColor = [UIColor grayColor];
    return cell;
    
}
- (IBAction)btnHomeClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
    [self.chatDelegate getStrChat:@"NO"];
}
- (IBAction)btnSettingClick:(id)sender {
}
- (IBAction)btnChatClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.chatDelegate getStrChat:@"YES"];
}
/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
