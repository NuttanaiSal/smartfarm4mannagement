//
//  TreeLogisTableViewCell.h
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TreeLogisTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTopic;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblGroup;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusWait;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusProcess;

@end

NS_ASSUME_NONNULL_END
