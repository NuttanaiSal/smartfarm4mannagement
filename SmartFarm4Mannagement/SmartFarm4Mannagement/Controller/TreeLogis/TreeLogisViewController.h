//
//  TreeLogisViewController.h
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TreeLogisTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@protocol chatDelegate
-(void)getStrChat:(NSString *)strChat;
@end

@interface TreeLogisViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrTreeLogis;
    NSMutableDictionary *dictTreeLogis;
    NSString *strTopic,*strDetail,*strGroup,*strStatusProcess,*strStatusWait;
    NSArray *arrTreeLogisRow;
    NSDictionary *dictTreeLogisRow;
}
@property (weak, nonatomic) id chatDelegate;
//header
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
- (IBAction)btnHomeClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
- (IBAction)btnSettingClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;
- (IBAction)btnChatClick:(id)sender;

//content
@property (weak, nonatomic) IBOutlet UITableView *tableTreeLogis;

@end

NS_ASSUME_NONNULL_END
