//
//  FundingViewController.m
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import "FundingViewController.h"

@interface FundingViewController ()

@end

@implementation FundingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setFunding];
    [self setButtonRadius:self.btnHome radius:90];
    [self setButtonRadius:self.btnSetting radius:90];
    [self setButtonRadius:self.btnChat radius:90];
}
-(void)setFunding{
    arrFunding = @[@"contentFunding1",@"contentFunding2"];
}
- (void)setButtonRadius: (UIButton *)button radius: (int)cornerRadius {
    button.layer.cornerRadius = cornerRadius;
    button.clipsToBounds = YES;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrFunding.count;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *simpleTableIdentifier = @"fundingCell";
    
    FundingCollectionViewCell *cell = (FundingCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    cell.imgFunding.image =[UIImage imageNamed:[arrFunding objectAtIndex:indexPath.row]];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.frame.size.width,collectionView.frame.size.height);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnHomeClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
    [self.chatDelegate getStrChat:@"NO"];
}
- (IBAction)btnSettingClick:(id)sender {
}
- (IBAction)btnChatClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.chatDelegate getStrChat:@"YES"];
}
@end
