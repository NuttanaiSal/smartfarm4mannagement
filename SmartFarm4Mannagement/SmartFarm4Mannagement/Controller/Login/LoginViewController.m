//
//  LoginViewController.m
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 11/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.conTopLoginView.constant = 275;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.conTopLoginView.constant = 400;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_txtfUser endEditing:YES];
    [_txtfPassword endEditing:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLogin:(id)sender {
    [self performSegueWithIdentifier:@"gotoMain" sender:self];
}
@end
