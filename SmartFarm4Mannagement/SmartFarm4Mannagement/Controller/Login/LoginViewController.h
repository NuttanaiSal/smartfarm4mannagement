//
//  LoginViewController.h
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 11/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController<UITextFieldDelegate>

//loginView
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conTopLoginView;//400<->250
@property (weak, nonatomic) IBOutlet UITextField *txtfUser;
@property (weak, nonatomic) IBOutlet UITextField *txtfPassword;
- (IBAction)btnLogin:(id)sender;

@end

NS_ASSUME_NONNULL_END
