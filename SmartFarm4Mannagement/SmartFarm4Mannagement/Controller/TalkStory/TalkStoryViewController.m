//
//  TalkStoryViewController.m
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import "TalkStoryViewController.h"

@interface TalkStoryViewController ()

@end

@implementation TalkStoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setData];
    [self setButtonRadius:self.btnHome radius:90];
    [self setButtonRadius:self.btnSetting radius:90];
    [self setButtonRadius:self.btnChat radius:90];
}
- (void)setButtonRadius: (UIButton *)button radius: (int)cornerRadius {
    button.layer.cornerRadius = cornerRadius;
    button.clipsToBounds = YES;
}
-(void)setData{
    arrData = @[@"talkStory1",@"talkStory2",@"talkStory1",@"talkStory1",@"talkStory1"];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *simpleTableIdentifier = @"talkStoryCell";
    TalkStoryTableViewCell *cell = (TalkStoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[TalkStoryTableViewCell alloc]initWithStyle:
        UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.imgTalkStory.image =[UIImage imageNamed:[arrData objectAtIndex:indexPath.row]];
    if (indexPath.row == 1) {
        cell.conhImgTalkStory.constant = 300;
    }else{
        cell.conhImgTalkStory.constant = 163;
    }
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnHomeClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
    [self.chatDelegate getStrChat:@"NO"];
}
- (IBAction)btnSettingClick:(id)sender {
}
- (IBAction)btnChatClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.chatDelegate getStrChat:@"YES"];
}
@end
