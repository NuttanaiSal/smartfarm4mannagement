//
//  MainViewController.h
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuCollectionViewCell.h"
#import "MainBanner1CollectionViewCell.h"
#import "MainBanner2CollectionViewCell.h"
#import "FundingViewController.h"
#import "ChatViewController.h"
#import "TreeLogisViewController.h"
#import "MemberEquipmentViewController.h"
#import "LearningViewController.h"
#import "TalkStoryViewController.h"
#import "ApaiViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface MainViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDragDelegate>
{
    NSArray *arrMenu,*arrBanner1,*arrBanner2;
    NSString *strGotoChat;
}

//header
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
- (IBAction)btnHomeClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
- (IBAction)btnSettingClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;
- (IBAction)btnChatClick:(id)sender;

//menu
@property (weak, nonatomic) IBOutlet UICollectionView *collectMenu;
@property (weak, nonatomic) IBOutlet UICollectionView *collectBanner1;
@property (weak, nonatomic) IBOutlet UICollectionView *collectBanner2;



@end

NS_ASSUME_NONNULL_END
