//
//  MainViewController.m
//  SmartFarm4Mannagement
//
//  Created by Nuttanai on 12/4/2563 BE.
//  Copyright © 2563 Nuttanai. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setMenu];
    [self setBanner];
    [self setButtonRadius:self.btnHome radius:90];
    [self setButtonRadius:self.btnSetting radius:90];
    [self setButtonRadius:self.btnChat radius:90];
    strGotoChat = @"NO";

}
- (void)viewDidAppear:(BOOL)animated{
    if ([strGotoChat isEqualToString:@"YES"]) {
        [self performSegueWithIdentifier:@"gotoChat" sender:self];
    }
}
-(void)getStrChat:(NSString *)strChat
{
    strGotoChat = strChat;
}
- (void)setButtonRadius: (UIButton *)button radius: (int)cornerRadius {
    button.layer.cornerRadius = cornerRadius;
    button.clipsToBounds = YES;
}
-(void)setMenu{
//    arrMenu = @[@"funding", @"treeLogis", @"memberEquipment", @"learning", @"talkStory", @"fromApai"];
    arrMenu = @[@"btn1", @"btn2", @"btn3", @"btn4", @"btn5", @"btn6"];
}
-(void)setBanner{
    arrBanner1 =@[@"banner1", @"banner1", @"banner1"];
    arrBanner2 =@[@"banner2", @"banner2", @"banner2"];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == _collectMenu) {
        return arrMenu.count;
    }else if (collectionView == _collectBanner1)
    {
        return arrBanner1.count;
    }
    else{
        return arrBanner2.count;
    }
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _collectMenu) {
        NSString *simpleTableIdentifier = @"menuCell";
        MenuCollectionViewCell *cell = (MenuCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        cell.imgMenu.image = [UIImage imageNamed:[arrMenu objectAtIndex:indexPath.row]];
        return cell;
    }else if (collectionView == _collectBanner1){
        NSString *simpleTableIdentifier = @"banner1Cell";
        MainBanner1CollectionViewCell *cell = (MainBanner1CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        cell.imgBanner.image = [UIImage imageNamed:[arrBanner1 objectAtIndex:indexPath.row]];
        return cell;
    }else{
        NSString *simpleTableIdentifier = @"banner2Cell";
        MainBanner2CollectionViewCell *cell = (MainBanner2CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        cell.imgBanner.image = [UIImage imageNamed:[arrBanner2 objectAtIndex:indexPath.row]];
        return cell;
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _collectMenu) {
        switch (indexPath.row) {
            case 0:
                [self performSegueWithIdentifier:@"gotoFunding" sender:self];
                break;
            case 1:
                [self performSegueWithIdentifier:@"gotoTreeLogis" sender:self];
                break;
            case 2:
                [self performSegueWithIdentifier:@"gotoMemberEquipment" sender:self];
                break;
            case 3:
            [self performSegueWithIdentifier:@"gotoLearning" sender:self];
            break;
            case 4:
            [self performSegueWithIdentifier:@"gotoTalkStory" sender:self];
            break;
            case 5:
            [self performSegueWithIdentifier:@"gotoApai" sender:self];
            break;
            default:
                break;
        }
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.frame.size.width,collectionView.frame.size.height);
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    float pageWidth;
    pageWidth = _collectBanner1.frame.size.width; // width + space
    float currentOffset = scrollView.contentOffset.x;
    float targetOffset = targetContentOffset->x;
    float newTargetOffset = 0;
    if (targetOffset > currentOffset)
        newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
    else
        newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
        
    if (newTargetOffset < 0)
        newTargetOffset = 0;
    else if (newTargetOffset > scrollView.contentSize.width)
        newTargetOffset = scrollView.contentSize.width;
    //NSInteger page = newTargetOffset / pageWidth;
    //_pageViewBanner1.currentPage = page;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"gotoFunding"]) {
        FundingViewController *FundingViewController =segue.destinationViewController;
        FundingViewController.chatDelegate = self;
    }
    if ([segue.identifier isEqualToString:@"gotoChat"]) {
        ChatViewController *ChatViewController =segue.destinationViewController;
        ChatViewController.chatDelegate = self;
    }
    if ([segue.identifier isEqualToString:@"gotoTreeLogis"]) {
        TreeLogisViewController *TreeLogisViewController =segue.destinationViewController;
        TreeLogisViewController.chatDelegate = self;
    }
    if ([segue.identifier isEqualToString:@"gotoMemberEquipment"]) {
        MemberEquipmentViewController *MemberEquipmentViewController =segue.destinationViewController;
        MemberEquipmentViewController.chatDelegate = self;
    }
    if ([segue.identifier isEqualToString:@"gotoLearning"]) {
        LearningViewController *LearningViewController =segue.destinationViewController;
        LearningViewController.chatDelegate = self;
    }
    if ([segue.identifier isEqualToString:@"gotoTalkStory"]) {
        TalkStoryViewController *TalkStoryViewController =segue.destinationViewController;
        TalkStoryViewController.chatDelegate = self;
    }
    if ([segue.identifier isEqualToString:@"gotoApai"]) {
        ApaiViewController *ApaiViewController =segue.destinationViewController;
        ApaiViewController.chatDelegate = self;
    }
    
}


- (IBAction)btnHomeClick:(id)sender {
}
- (IBAction)btnSettingClick:(id)sender {
}
- (IBAction)btnChatClick:(id)sender {
    [self performSegueWithIdentifier:@"gotoChat" sender:self];
}
@end
